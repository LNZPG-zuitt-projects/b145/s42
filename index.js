/* Discussion */

// // alert('hi');


// console.log(document);


// // Target first name input field

// // document.querySelector('#txt-first-name');

// // alternative:
// // document.getElementById('txt-first-name');

// const txtFirstName = document.querySelector("#txt-first-name");

// const spanFullName = document.querySelector("#span-full-name");



// // Event Listeners

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });


// // Multiple Listeners
// 	// e is for event
// txtFirstName.addEventListener('keyup', (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });




// ACTIVITY


// Target first name input field

// document.querySelector('#txt-first-name');

// alternative:
// document.getElementById('txt-first-name');


// DEFINED
const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");


// DISPLAY ON  THE SPAN
const updateFullName = () => {

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', (updateFullName));
txtLastName.addEventListener('keyup', (updateFullName));




// Multiple Listeners
	// e is for event
txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value);
});

txtLastName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value);
});